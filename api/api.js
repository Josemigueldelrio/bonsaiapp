import express from "express";
import cors from "cors";

const app = express();
app.use(cors());
app.use(express.json({ limit: "50mb" }));

const port = process.env.PORT || 3001;

app.listen(port, () => {
  console.log(`Server started at localhost: ${port}`);
});

app.get("/mock", (req, res) => {
  res.json({ prueba: "superada" });
});
